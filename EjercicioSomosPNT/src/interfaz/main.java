package interfaz;

import solucion.Solucion;


public class main {

	public static void main(String[] args) {
		Solucion _solucion = new Solucion();
		_solucion.cargarDatos();
		_solucion.mostrasDetalles();
		System.out.println("===========================");
		System.out.println("Producto m�s caro: "+ _solucion.masCaro());
		System.out.println("Producto m�s barato: "+ _solucion.masBarato());
		
	}

}
