package solucion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import datos.Bebida;
import datos.Fruta;
import datos.Limpieza;
import datos.Producto;

public class Solucion {
	ArrayList<Producto> _lista;
	public Solucion() {
		_lista = new ArrayList<>();
	}
	
	public void cargarDatos() 
	{
		_lista.add(new Bebida("Coca-Cola",1.2,18));
		_lista.add(new Bebida("Coca-Cola Zero",1.2,20));
		_lista.add(new Limpieza("Shampoo Sedal","500ml", 19));
		_lista.add(new Fruta("Frutilla",64,"kilo"));
	}

	public void mostrasDetalles() {
		for(Producto p: _lista) {
			System.out.println(p.toString());
		}
		
	}

	public String masCaro() {
		 return Collections.max(_lista, precio(_lista))._nombre;
		
		
	}

	private Comparator<Producto> precio(ArrayList<Producto> _lista) {
		return (uno, otro) -> uno._precio - otro._precio;
	}

	public String masBarato() {
		return Collections.min(_lista,precio(_lista))._nombre ;
	}
}