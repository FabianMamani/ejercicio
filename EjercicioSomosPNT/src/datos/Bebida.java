package datos;

public class Bebida extends Producto{
	private double _litros;
	public Bebida(String nombre,double litros, int precio) {
		_litros = litros;
		super._nombre = nombre;
		super._precio = precio;
	}
	
	public String getNombre() {
		return super._nombre;
	}
	@Override
	public String toString() {
		return "Nombre: "+ super._nombre+" /// "+
				"Litros: "+ _litros+ " /// "+
				"Precio: "+super._precio;
		
	}
}
