package datos;

public class Fruta extends Producto{
	private String _unidadVenta;
	public Fruta(String nombre,int precio,String unidad_venta) {
		super._nombre= nombre;
		super._precio = precio;
		_unidadVenta = unidad_venta;
	}
	
	public String getNombre() {
		return super._nombre;
	}
	
	@Override
	public String toString() {
		return "Nombre: "+super._nombre+" /// "
				+"Precio: "+super._precio+" /// "
				+"Unidad de Venta: "+_unidadVenta;
	}


}
