package datos;

public class Limpieza extends Producto {
	private String _contenido;
	public Limpieza(String nombre, String contenido, int precio) {
		super._nombre= nombre;
		_contenido = contenido;
		super._precio= precio;
	}
	
	public String getNombre() {
		return super._nombre;
	}
	@Override
	public String toString() {
		return "Nombre: "+super._nombre+" /// "+
				"Contenido: "+_contenido+" /// "
				+"Precio: "+super._precio;
	}

}
