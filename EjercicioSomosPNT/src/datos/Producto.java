package datos;

public class Producto implements Comparable<Producto>
	{
	
	public String _nombre;
	public int _precio;
	
	@Override
	public String toString() {
		return "Nombre: " + _nombre +"///"
				+"Precio: " + _precio+ "///";
	}

	@Override
	public int compareTo(Producto p) {
		if(_precio > p._precio) {
			return -1;
		}else if(_precio < p._precio){
			return 1;
		}
		return 0;
	}
	

}
